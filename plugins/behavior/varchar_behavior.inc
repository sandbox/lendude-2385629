<?php
  
/**
 * @file
 * Adds a entityreference behaviour via CTools plugin.
 */
 
$plugin = array(
  'title' => t('Convert to varchar field'),
  'class' => 'EntityReferenceFieldVarcharField',
  'weight' => 10,
  'behavior type' => 'field',
);
