<?php

/**
 * @file
 * CTool plugin class for entityreference bahaviour.
 */

class EntityReferenceFieldVarcharField extends EntityReference_BehaviorHandler_Abstract {

  public function schema_alter(&$schema, $field) {
    if (isset($field['settings']['handler_settings']['behaviors']['varchar_behavior']) && $field['settings']['handler_settings']['behaviors']['varchar_behavior']) {
      $schema['columns']['target_id']['type'] = 'varchar';
      $schema['columns']['target_id']['length'] = 255;
      unset($schema['columns']['target_id']['unsigned']);
    }
  }
  
  public function is_empty_alter(&$empty, $item, $field) {
    if (isset($field['settings']['handler_settings']['behaviors']['varchar_behavior']) && $field['settings']['handler_settings']['behaviors']['varchar_behavior']) {
      if ($empty && $item) $empty = FALSE;
    }
  }
}
