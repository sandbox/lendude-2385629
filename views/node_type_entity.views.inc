<?php

/**
 * @file
 * Adds the right views info for the node_type entity.
 */
  
/**
 * Implements hook_views_data_alter().
 */
function node_type_entity_views_data_alter(&$data) {
  // Add a relationship handler for the node type.
  $data['node']['type']['relationship'] = array(
    'title' => t('Node type'),
    'help' => t('Relate content to the node type of the content.'),
    'handler' => 'views_handler_relationship',
    'base' => 'node_type',
    'field' => 'type',
    'label' => t('node type'),
  ); 
}